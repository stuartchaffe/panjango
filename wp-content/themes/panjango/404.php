<?php get_header(); ?>
	<main class="section-orange">
		<div class="wrapper">
			<div class="row">
				<div class="col-12 col-sm-8 offset-md-2">
                    <h1>Page not found (404)</h1>
                    <p>We're sorry the page you requested cannot be found.</p>
				</div>
			</div>
		</div>
	</main>

<?php get_footer(); ?>