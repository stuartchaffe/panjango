<?php

/**
 * Remove unnecessary meta/link tags
 */
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links', 2 );
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');	
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('wp_head', 'rest_output_link_wp_head', 10);
remove_action('wp_head', 'wp_oembed_add_discovery_links', 10);



/**
 * Add featured image support
 */
add_theme_support('post-thumbnails');

add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );

function remove_width_attribute( $html ) {
   $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
   return $html;
}

/**
 * Add admin sidebar
 *
 */

// add_action( 'init', 'create_post_type' );
// function create_post_type() {
//     register_post_type( 'testimonials',
//     array(
//       'labels' => array(
//       'name' => 'Testimonials',
//       'add_new' => 'Add New',
//       'add_new_item' => 'Add New',
//       'singular_name' => 'Testimonial'
//       ),
//     'public' => true,
//     'has_archive' => false,
//     'hierarchical' => true,
//     'rewrite' => array('slug' => 'testimonials'),
//     )
//   );
// }