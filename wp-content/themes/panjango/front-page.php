<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
	<section class="banner banner--home">
		<div class="wrapper">
			<div class="row">
				<div class="col-12">
					<h1><?php the_field('banner_title');?></h1>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-sm-7 col-lg-6">
					<?php the_field('banner_content');?>
				</div>
			</div>
		</div>
		<div class="banner--home__image">
			<img src="<?php echo get_template_directory_uri() ?>/images/hero-banner-home.svg" alt="Panjango learning" />
		</div>
	</section>

	<section class="video">
		<div class="wrapper">
			<div class="row justify-center">
				<div class="col-12 col-sm-10 col-md-9 col-lg-8 col-xl-7">
					<?php the_field('video_content');?>
				</div>
			</div>
		<?php if( get_field('video_url') ): ?>
			<div class="video--container__video">
				<?php the_field('video_url');?>
			</div>
		<?php else: ?>
			<div class="video--container__placeholder">
				<p>Video coming very soon... <br />watch this space.</p>
			</div>
		<?php endif; ?>
			<div class="video--container">
				<img class="video--container__image" src="<?php echo get_template_directory_uri() ?>/images/video-guy.svg" alt="Panjango video" />
			</div>
		<?php if( get_field('video_link_name') ): ?>
			<a href="<?php the_field('video_link_url');?>" class="btn"><?php the_field('video_link_name');?></a>
		<?php endif; ?>
		</div>
	</section>

	<section class="products section-blue">
		<div class="wrapper">
			<h2>Our Products</h2>

			<div class="products-list">
				<a href="/products/panjango-app" class="col-12 col-sm-3 products-list--item">
					<div class="products-list--item__image">
						<img class="image-app" src="<?php echo get_template_directory_uri() ?>/images/panjango-app.svg" alt="Panjango app" />
					</div>
					<span class="h3">Panjango<br /> App →</span>
				</a>
				<a href="/products/panjango-for-schools" class="col-12 col-sm-3 products-list--item">
					<div class="products-list--item__image">
						<img class="image-schools" src="<?php echo get_template_directory_uri() ?>/images/panjango-for-schools.svg" alt="Panjango app" />
					</div>
					<span class="h3">Panjango<br /> for Schools →</span>
				</a>
				<a href="/panjango-board-card-games" class="col-12 col-sm-3 products-list--item">
					<div class="products-list--item__image">
						<img class="image-cards" src="<?php echo get_template_directory_uri() ?>/images/panjango-cards.svg" alt="Panjango app" />
					</div>
					<span class="h3">Board &amp;<br /> Card Games →</span>
				</a>
			</div>
		</div>
		<div class="angle angle-bottom angle-grey"></div>
	</section>

	<section class="social-purpose">
		<div class="wrapper">
			<h2><?php the_field('social_purpose_title');?></h2>

			<div class="row">
				<div class="col-3 col-md-2 offset-md-1">
					<img class="image-plant" src="<?php echo get_template_directory_uri() ?>/images/plant-grown.svg" alt="Panjango plant grown" />
				</div>
				<div class="col-12 col-md-7 offset-md-2">
					<img class="image-watering" src="<?php echo get_template_directory_uri() ?>/images/watering-can.svg" alt="Panjango watering plants" />
					<?php the_field('social_purpose_content');?>
				</div>
			</div>
		</div>
	</section>

	<section class="carousel-testimonials section-yellow">
		<div class="angle angle-top angle-down angle-yellow"></div>
		<div class="wrapper">
			<div class="testimonial-slider">
			<?php if( have_rows('testimonials') ): ?>
                <?php while ( have_rows('testimonials') ) : the_row(); ?>
                <div class="carousel-testimonials--item">
                    <?php if ( get_sub_field( 'testimonials_avatar' ) ): ?>
                        <div class="carousel-testimonials--item__image">
                            <img src="<?php the_sub_field('testimonials_avatar');?>" alt="Author image" />
                        </div>
                    <?php endif; ?>
                    <?php if ( get_sub_field( 'testimonial_content' ) ): ?>
                        <div class="carousel-testimonials--item__content">
                            <span class="content"><?php the_sub_field('testimonial_content'); ?></span>
                            <?php the_sub_field('testimonial_author'); ?>
                        </div>
                    <?php endif; ?>
                </div>
                <?php endwhile; ?>
			<?php endif; ?>
			</div>
		</div>
	</section>

	<section class="supported">
		<div class="wrapper">
			<div class="row justify-center">
				<div class="col-12 col-md-7">
					<h2><?php the_field('supported_title');?></h2>
				</div>
				<div class="col-12">
					<div class="supported-slider">
					<?php if( have_rows('supported_logos') ): ?>
					<?php while ( have_rows('supported_logos') ) : the_row(); ?>
						<?php if ( get_sub_field( 'supported_image' ) ): ?>
							<div><img src="<?php the_sub_field('supported_image');?>" alt="Supported logo" /></div>
							<?php endif; ?>
						<?php endwhile; ?>
					<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="newsletter">
		<div class="wrapper">
			<div class="row">
				<div class="col-8 col-sm-6 col-md-5 offset-2 offset-sm-3 offset-md-0">
					<img src="<?php echo get_template_directory_uri() ?>/images/pencil-man.svg" alt="Panjango pencil man" />
				</div>
				<div class="col-12 col-md-6 offset-md-1">
					<p><strong>Sign up for all the latest news, insights and product updates from the Panjango team.</strong></p>

					<div class="standard--form">
						<!-- Begin MailChimp Signup Form -->
						<div id="mc_embed_signup">
							<form action="https://online.us12.list-manage.com/subscribe/post?u=45c3fa40e301c494caf49f8ed&amp;id=7031080039" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
								<div id="mc_embed_signup_scroll">
								
							<div class="standard--form__field">
								<label for="mce-NAME">Name </label>
								<input type="text" value="" name="NAME" class="" id="mce-NAME">
							</div>
							<div class="standard--form__field">
								<label for="mce-EMAIL">Email </label>
								<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL">
							</div>
								<div id="mce-responses" class="clear">
									<div class="response" id="mce-error-response" style="display:none"></div>
									<div class="response" id="mce-success-response" style="display:none"></div>
								</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
								<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_45c3fa40e301c494caf49f8ed_7031080039" tabindex="-1" value=""></div>
								<input type="submit" value="Sign up" name="subscribe" id="mc-embedded-subscribe" class="btn">
								</div>
							</form>
						</div>
						<!--End mc_embed_signup-->
					</div>
				</div>
		</div>
	</section>

<?php endwhile; ?>

<?php get_footer(); ?>