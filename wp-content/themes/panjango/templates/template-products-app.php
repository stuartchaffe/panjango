<?php

// Template Name: Products app

the_post();
get_header(); 
?>

    <section class="products products-app">
		<div class="wrapper">
            <section class="app-video">
                <div class="app-video--container">
                    <div class="row justify-center">
                        <div class="col-10">
                    <?php if( get_field('video_url') ): ?>
                        <div class="app-video--container__video">
                            <?php the_field('video_url');?>
                        </div>
                    <?php else: ?>
                        <div class="video--container__placeholder">
                            <p>Video coming very soon... <br />watch this space.</p>
                        </div>
                    <?php endif; ?>
                        </div>
                    </div>
                    <img class="app-video--container__image" src="<?php echo get_template_directory_uri() ?>/images/shelf.svg" alt="Shelf" />
                </div>
            </section>
        
            <h1><?php the_title(); ?></h1>

            <div class="row">
                <div class="col-12 col-sm-4 image-app">
                    <img src="<?php echo get_template_directory_uri() ?>/images/panjango-app.svg" alt="Panjango app" />
                </div>
                <div class="col-12 col-sm-8">
                    <?php the_field('app_content');?>
                <?php if( get_field('app_link_name') ): ?>
                    <a class="btn" href="<?php the_field('app_link_url');?>"><?php the_field('app_link_name');?></a>
                <?php endif; ?>
                </div>
            </div>
		</div>
	</section>

<?php get_footer(); ?>