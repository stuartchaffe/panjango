<?php

// Template Name: Products card games

the_post();
get_header(); 
?>

    <section class="products-page products-games">
		<div class="wrapper">
            <h1><?php the_title(); ?></h1>
            <div class="row">
                <div class="col-12 col-sm-7">
                    <?php the_field('trumps_content_block_1');?>
                    <a class="btn" href="<?php the_field('trumps_link_url');?>"><?php the_field('trumps_link_name');?></a>
                </div>
                <div class="col-12 col-sm-5 image-resize">
                    <img src="<?php echo get_template_directory_uri() ?>/images/panjango-cards.svg" alt="Panjango cards" />
                </div>

                <div class="col-12 col-sm-8 offset-sm-4 u-mt5">
                    <?php the_field('trumps_content_block_2');?>
                </div>
            </div>
        </div>
    </section> 
    
    <section class="products products-games--board section-orange">
        <div class="angle angle-top angle-down angle-orange"></div>
		<div class="wrapper">
            <div class="row">
                <div class="col-12 col-sm-7">
                    <?php the_field('board_content_block_1');?>
                    <a class="btn" href="<?php the_field('trumps_link_url');?>"><?php the_field('trumps_link_name');?></a>
                </div>
                <div class="col-12 col-sm-5 image-resize">
                    <img src="<?php echo get_template_directory_uri() ?>/images/board-game.svg" alt="Panjango board game" />
                </div>

                <div class="col-12 col-sm-8 offset-sm-4 u-mt5">
                    <?php the_field('board_content_block_2');?>
                </div>
            </div>
        </div>
	</section> 

<?php get_footer(); ?>