<?php

// Template Name: About

the_post();
get_header(); 
?>

    <section class="banner banner--about section-grey">
		<div class="wrapper">
            <div class="row">
                <div class="col-12 col-md-6">
                    <img src="<?php echo get_template_directory_uri() ?>/images/hero-banner-about.svg" alt="About Panjango" />
                </div>
                <div class="col-12 col-md-5 offset-md-1">
                    <h1><?php the_field('banner_title');?></h1>
                    <?php the_field('banner_content');?>
                </div>
            </div>
		</div>
    </section>
    
    <section class="values">
		<div class="wrapper">
			<div class="row">
				<div class="col-12 col-sm-4 col-lg-3">
					<?php the_field('content_left');?>
				</div>
				<div class="col-12 col-sm-7 col-lg-8 offset-sm-1">
					<?php the_field('content_right');?>
				</div>
            </div>
        </div>
        <img src="<?php echo get_template_directory_uri() ?>/images/values.svg" alt="Panjango values" />
    </section>
    
    <section class="team">
		<div class="wrapper">
            <h2><?php the_field('team_title');?></h2>

        <?php if( have_rows('team_member') ): ?>
            <?php while ( have_rows('team_member') ) : the_row(); ?>
            <div class="team-member">
                <div class="row align-bottom">
                    <div class="col-12 col-sm-4 col-md-5 col-lg-3 team-member--image">
                    <?php if ( get_sub_field( 'team_avatar' ) ): ?>
                        <img src="<?php the_sub_field('team_avatar');?>" alt="<?php the_sub_field('team_name'); ?>" />
                    <?php endif; ?>
                    </div>
                    <div class="col-12 col-sm-7 col-md-6 col-lg-8">
                        <div class="team-member--content">
                        <?php if ( get_sub_field( 'team_name' ) ): ?>
                            <h3><?php the_sub_field('team_name'); ?></h3>
                        <?php endif; ?>
                        <?php if ( get_sub_field( 'team_position' ) ): ?>
                            <p><strong><?php the_sub_field('team_position'); ?></strong></p>
                        <?php endif; ?>
                        <?php if ( get_sub_field( 'team_bio' ) ): ?>
                            <?php the_sub_field('team_bio'); ?>
                        <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>
        <?php endif; ?>


            <div class="team-member">
                <div class="row align-bottom">
                    <div class="col-12 col-sm-4 col-md-5 col-lg-4 team-member--image-bob">
                    <?php if( get_field('bob_avatar') ): ?>
                        <img src="<?php the_field('bob_avatar'); ?>" alt="<?php the_field('bob_title');?>" />
                    <?php endif; ?>
                    </div>
                    <div class="col-12 col-sm-7 col-md-6 col-lg-8">
                        <div class="team-member--content">
                        <?php if ( get_field( 'bob_title' ) ): ?>
                            <h3><?php the_field('bob_title');?></h3>
                        <?php endif; ?>
                        <?php if ( the_field('bob_content' ) ): ?>
                            <p><strong><?php the_field('bob_content');?></strong></p>
                        <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
		</div>
    </section>
    
    <section class="ambassadors">
		<div class="wrapper">
            <h2><?php the_field('ambassador_title');?></h2>
            <div class="ambassadors-list">
            <?php if( have_rows('ambassador') ): ?>
                <?php while ( have_rows('ambassador') ) : the_row(); ?>
                <div class="ambassadors-list--item">
                    <div class="ambassadors-list--item__image">
                    <?php if ( get_sub_field( 'ambassador_avatar' ) ): ?>
                        <img src="<?php the_sub_field('ambassador_avatar');?>" alt="<?php the_sub_field('ambassador_name'); ?>" />
                    <?php endif; ?>
                    </div>
                    <div class="ambassadors-list--item__content">
                    <?php if ( get_sub_field( 'ambassador_name' ) ): ?>
                        <p><strong><?php the_sub_field('ambassador_name'); ?></strong></p>
                    <?php endif; ?>
                    <?php if ( get_sub_field( 'ambassador_description' ) ): ?>
                            <p><?php the_sub_field('ambassador_description'); ?></p>
                    <?php endif; ?>
                    </div>  
                </div>
                <?php endwhile; ?>
            <?php endif; ?>
            </div>
		</div>
    </section>
    
    <section class="foundation section-green">
        <div class="angle angle-top angle-down angle-green"></div>
		<div class="wrapper">
            <div class="row align-center">
				<div class="col-12 col-md-5">
                    <h2 class="u-left-align"><?php the_field('foundation_title');?></h2>
                    <?php the_field('foundation_content');?>
                </div>

                <div class="col-12 col-md-6 offset-md-1">
                    <img src="<?php echo get_template_directory_uri() ?>/images/foundation.svg" alt="Panjango foundation" />
                </div>
            </div>
        </div>
    </section>

    <section class="media-kit section-orange">
        <div class="angle angle-top angle-up angle-orange"></div>
		<div class="wrapper">
            <div class="row align-center">
				<div class="col-12 col-md-6 offset-md-1 order-md-2">
                    <h2 class="u-left-align"><?php the_field('media_kit_title');?></h2>
                    <?php the_field('media_kit_content');?>
                </div>
                <div class="col-12 col-md-5 u-center order-md-1">
                    <img src="<?php echo get_template_directory_uri() ?>/images/media-kit.svg" alt="Media Kit" />
                <?php if( get_field('media_kit_link_name') ): ?>
                    <a class="btn" href="<?php the_field('media_kit_link_url');?>"><?php the_field('media_kit_link_name');?></a>
                <?php endif; ?>
                </div>
            </div>
		</div>
    </section>

<?php get_footer(); ?>