<?php

// Template Name: Products landing

the_post();
get_header(); 
?>

    <section class="products-page section-blue">
		<div class="wrapper">
            <h1><?php the_title(); ?></h1>

			<div class="products-list">
                <div class="col-12 col-sm-3">
                    <a href="/products/panjango-app" class="products-list--item">
                        <div class="products-list--item__image">
                            <img class="image-app" src="<?php echo get_template_directory_uri() ?>/images/panjango-app.svg" alt="Panjango app" />
                        </div>
                        <span class="h3"><?php the_field('product_name_1');?> →</span>
                    </a>
                    <?php the_field('product_description_1');?>
                </div>
				<div class="col-12 col-sm-3">
                    <a href="/products/panjango-for-schools" class="products-list--item">
                        <div class="products-list--item__image">
                            <img class="image-schools" src="<?php echo get_template_directory_uri() ?>/images/panjango-for-schools.svg" alt="Panjango app" />
                        </div>
                        <span class="h3"><?php the_field('product_name_2');?> →</span>
                    </a>
                    <?php the_field('product_description_2');?>
                </div>
				<div class="col-12 col-sm-3">
                    <a href="/products/panjango-board-card-games" class="products-list--item">
                        <div class="products-list--item__image">
                            <img class="image-cards" src="<?php echo get_template_directory_uri() ?>/images/panjango-cards.svg" alt="Panjango app" />
                        </div>
                        <span class="h3"><?php the_field('product_name_3');?> →</span>
                    </a>
                    <?php the_field('product_description_3');?>
                </div>
			</div>
		</div>
	</section>

<?php get_footer(); ?>