<?php

// Template Name: Blog

the_post();
get_header(); 
?>

    <section class="blog">
        <div class="wrapper">
            <div class="row">
                <div class="col-12 col-md-7 col-lg-6">
                    <h2><?php the_field('blog_title');?></h2>
                </div>
                <div class="col-12 offset-sm-0 col-md-5 col-lg-5 offset-lg-1">
                    <img class="image-computer" src="<?php echo get_template_directory_uri() ?>/images/computer-operator.svg" alt="Panjango computer operator" />
                </div>
            </div>
        </div>
    </section>
    <section class="blog-list">
        <div class="angle angle-top angle-down angle-blue"></div>
        <div class="wrapper">
                <?php get_template_part( 'partials/loop', 'post' ); ?>
        </div>
    </section>

   
<?php get_footer(); ?>