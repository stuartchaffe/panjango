<?php

// Template Name: Jobs

the_post();
get_header(); 
?>

	<section class="jobs">
        <div class="wrapper">
            <div class="row">
                <div class="col-12 col-md-7 col-lg-6">
                    <h2><?php the_field('title');?></h2>
                </div>
                <div class="col-12 offset-sm-0 col-md-5 col-lg-5 offset-lg-1">
                    <img class="image-jobs" src="<?php echo get_template_directory_uri() ?>/images/jobs-guys.svg" alt="Panjango jobs" />
                </div>
            </div>
        </div>
    </section>
    
	
	<section class="jobs-list">

        <div class="wrapper">
            <div class="row">
                <div class="col-12 col-md-4">
                    <?php the_field('jobs_list_content');?>
                </div>
                <div class="col-12 col-md-8">
                    <div id="accordion">
                    <?php if ( have_rows('jobs_list') ) : ?>
                        <?php while ( have_rows('jobs_list') ) : the_row(); ?>
                        <div class="accordion--item">
                            <div class="accordion--item__toggle">
                                <?php the_sub_field('job_title');?>
                            </div>
                            <div class="accordion--item__content">
                                <?php the_sub_field('description');?>
                            </div>
                        </div>
                    <?php endwhile;
                    endif; ?>
                    </div>

                    <div class="accordion" style="display: none;">
                    <?php if ( have_rows('jobs_list') ) : $i = 0; 
                        while ( have_rows('jobs_list') ) : the_row(); $i++;?>
                        <div class="tab">
                            <input id="tab-<?php echo $i; ?>" type="checkbox" name="tabs">
                            <label for="tab-<?php echo $i; ?>"><?php the_sub_field('job_title');?></label>
                            <div class="tab-content">
                                <p><?php the_sub_field('description');?></p>
                            </div>
                        </div>   
                    <?php endwhile;
                    endif; ?>
                    </div>
                </div>
            </div>
            <img class="image-job-people" src="<?php echo get_template_directory_uri() ?>/images/job-people.svg" alt="Panjango jobs" />
        </div>
    </section>

<?php get_footer(); ?>