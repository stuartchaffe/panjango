<?php

// Template Name: Products for schools

the_post();
get_header(); 
?>

    <section class="products-page products-schools">
		<div class="wrapper">
            <h1><?php the_title(); ?></h1>

            <div class="row">
                <div class="col-12 col-sm-8 offset-sm-4">
                    <?php the_field('content_block_1');?>
                </div>
            </div>

            <div class="row">
                <img class="image-dreaming" src="<?php echo get_template_directory_uri() ?>/images/day-dreaming.svg" alt="Day dreaming" />
                <div class="col-12 col-sm-7 offset-sm-5 u-mt3">
                    <?php the_field('content_block_2');?>
                </div>
            </div>

            <img class="image-schools" src="<?php echo get_template_directory_uri() ?>/images/panjango-for-schools.svg" alt="Day dreaming" />
        </div>
    </section>
    
    <section class="products-challenges section-yellow">
        <div class="wrapper">
            <div class="angle angle-top angle-up angle-yellow"></div>
            <div class="row">
                <div class="col-12 col-sm-4 mobile-hide">
                    <img class="image-secretary" src="<?php echo get_template_directory_uri() ?>/images/secretary.svg" alt="Secretary" />
                    <img class="image-playing" src="<?php echo get_template_directory_uri() ?>/images/playing-cards.svg" alt="Playing Panjango" />
                    <img class="image-mirror" src="<?php echo get_template_directory_uri() ?>/images/mirror.svg" alt="Looking in the mirror" />
                </div>
                <div class="col-12 col-sm-8">
                    <?php the_field('challenges_content');?>
                    <a class="btn" href="<?php the_field('challenges_link_url');?>"><?php the_field('challenges_link_name');?></a>
                    <img class="image-apple" src="<?php echo get_template_directory_uri() ?>/images/apple.svg" alt="Apple" />
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>