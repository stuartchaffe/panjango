<?php

// Template Name: Contact

the_post();
get_header(); 
?>

	<section class="contact">
		<div class="wrapper">
			<img class="contact--image" src="<?php echo get_template_directory_uri() ?>/images/talking-heads.svg" alt="Panjango talking" />
		</div>
	</section>

	<section class="contact-details section-yellow">
		<div class="angle angle-top angle-down angle-yellow"></div>
		<div class="wrapper">
			<div class="row">
				<div class="col-12 col-sm-5">
					<h3><?php the_field('content_left');?></h3>
				</div>
				<div class="col-12 col-sm-7">
					<?php the_field('content_right');?>
					<div class="standard--form">
						<!-- Begin MailChimp Signup Form -->
						<div id="mc_embed_signup">
							<form action="https://online.us12.list-manage.com/subscribe/post?u=45c3fa40e301c494caf49f8ed&amp;id=7031080039" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
								<div id="mc_embed_signup_scroll">
								
								<div class="standard--form__field">
									<label for="mce-NAME">Name </label>
									<input type="text" value="" name="NAME" class="" id="mce-NAME">
								</div>
								<div class="standard--form__field">
									<label for="mce-EMAIL">Email </label>
									<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
								</div>
								<div class="standard--form__field">
									<label for="mce-MMERGE2">Phone Number </label>
									<input type="text" name="MMERGE2" class="" value="" id="mce-MMERGE2">
								</div>
								<div class="standard--form__field">
									<label for="mce-MESSAGE">Message </label>
									<input type="text" value="" name="MESSAGE" class="" id="mce-MESSAGE">
								</div>
									<div id="mce-responses" class="clear">
										<div class="response" id="mce-error-response" style="display:none"></div>
										<div class="response" id="mce-success-response" style="display:none"></div>
									</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
									<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_45c3fa40e301c494caf49f8ed_7031080039" tabindex="-1" value=""></div>
									<div class="clear"><input type="submit" value="Send" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
									</div>
								</form>
						</div>
						<!--End mc_embed_signup-->
					</div>
				</div>
			</div>
		</div>
	</section>

<?php get_footer(); ?>