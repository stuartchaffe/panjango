<?php

// Template Name: Testimonials

the_post();
get_header(); 
?>

	<section class="testimonials">
		<div class="wrapper">
			<h1><?php the_title(); ?></h1>
            <?php if( have_rows('testimonials') ): ?>
                <?php while ( have_rows('testimonials') ) : the_row(); ?>
                <div class="testimonials--item">
                    <?php if ( get_sub_field( 'testimonials_avatar' ) ): ?>
                        <div class="testimonials--item__image">
                            <img src="<?php the_sub_field('testimonials_avatar');?>" alt="Author image" />
                        </div>
                    <?php endif; ?>
                    <?php if ( get_sub_field( 'testimonial_content' ) ): ?>
                        <div class="testimonials--item__content">
                            <span class="content"><?php the_sub_field('testimonial_content'); ?></span>
                            <?php the_sub_field('testimonial_author'); ?>
                        </div>
                    <?php endif; ?>
                </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
        <div class="testimonials--background section-orange">
            <div class="angle angle-top angle-down angle-orange"></div>
            <div class="angle angle-bottom angle-up angle-green"></div>
        </div>
    </section>
    <section class="testimonials-kids section-orange">
        <div class="angle angle-top angle-down angle-orange"></div>
		<div class="wrapper">
            <div class="row justify-center">
                <div class="col-12 col-sm-10">
                <?php if ( get_field( 'kids_testimonial' ) ): ?>
                    <div class="testimonials-kids--item">
                        <div class="testimonials-kids--item__content">
                            <span class="content"><?php the_field('kids_testimonial_content'); ?></span>
                            <?php the_field('kids_testimonial_author'); ?>
                        </div>
                        <img class="image-testimonials-kids" src="<?php echo get_template_directory_uri() ?>/images/testimonial-kids.svg" alt="Panjango app" />
                    </div>
                <?php endif; ?>
            </div>
		</div>
    </section>

<?php get_footer(); ?>