<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
	<article>
		<div class="blog-header">
			<div class="wrapper">
				<a href="/blog">← Back to blog list</a>
				<div class="row">
					<div class="col-12 col-sm-12 col-md-10 col-lg-8">
						<div class="time-date"><?php the_time('d/m/Y') ?></div>
						<h1><?php the_title(); ?></h1>					
					</div>
				</div>
			</div>
			<div class="angle angle-bottom angle-up angle-grey"></div>
		</div>

		<div class="wrapper">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-10 col-lg-8">
					<?php the_content(); ?>
				</div>
			</div>
			<img class="image-apple" src="<?php echo get_template_directory_uri() ?>/images/apple.svg" alt="Apple" />
		</div>
		
	</article>
	
<?php endwhile; ?>

<?php get_footer(); ?>