<footer role="contentinfo">
	<div class="wrapper">
		<a class="logo" href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri() ?>/images/logo.svg" alt="Panjango Logo" /></a>
		
		
		<div class="footer-links">
			<nav><?php wp_nav_menu( array('theme_location' => 'footer') ); ?></nav>

			<div class="footer-social">
				<a href="https://www.facebook.com/panjango/"><img src="<?php echo get_template_directory_uri() ?>/images/icon-social-facebook.svg" alt="Facebook Logo" /></a>
				<a href="https://twitter.com/Panjango"><img src="<?php echo get_template_directory_uri() ?>/images/icon-social-twitter.svg" alt="Twitter Logo" /></a>
				<a href=""><img src="<?php echo get_template_directory_uri() ?>/images/icon-social-instagram.svg" alt="Instagram Logo" /></a>
				<a href=""><img src="<?php echo get_template_directory_uri() ?>/images/icon-social-youtube.svg" alt="Youtube Logo" /></a>
			</div>
		</div>
	</div>
</footer><!-- /page-footer -->

</div><!-- /wrapper -->

<?php wp_footer(); ?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119026856-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-119026856-1');
</script>

</body>
</html>