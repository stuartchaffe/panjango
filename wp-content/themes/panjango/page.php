<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
	<main>
		<div class="wrapper">
			<div class="row">
				<div class="col-12 col-sm-8 offset-md-2">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</main>
<?php endwhile; ?>

<?php get_footer(); ?>